import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../services/auth.service';

@Injectable()
export class GuestGuard implements CanActivate {

  constructor(protected router: Router, protected authService: AuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const isNotAuthenticated = !this.authService.isLoggedIn();
    console.log(isNotAuthenticated);
    if (isNotAuthenticated) {
      return true;
    }

    this.router.navigate(['/']);

    return isNotAuthenticated;
  }
}
