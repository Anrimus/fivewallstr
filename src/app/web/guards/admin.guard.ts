import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User} from '../models/User';
import {AuthService} from '../services/auth.service';

@Injectable()
export class AdminGuard implements CanActivate {
  public user: User = null;
  constructor(public authService: AuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.user = this.authService.getFromLocalStorage();
    return this.user.isAdmin;
  }
}
