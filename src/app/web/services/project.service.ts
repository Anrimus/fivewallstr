import {EventEmitter, Injectable} from '@angular/core';
import {Project} from '../models/Project';
import {Observable} from 'rxjs/Observable';
import PROJECTS from '../mocks/projects';

@Injectable()
export class ProjectService {

  public onProjectCreate$ = new EventEmitter();
  public projects: Array<Project>;

  constructor() {
    this.projects = PROJECTS;
  }

  public getProjects() {
    return Observable.of(this.projects);
  }

  public createNewProject(project) {
    const data = new Project(project);
    this.projects.push(data);
  }
}
