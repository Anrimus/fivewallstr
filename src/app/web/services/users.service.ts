import {EventEmitter, Injectable} from '@angular/core';
import {User} from '../models/User';
import {Observable} from 'rxjs/Observable';

import mock from '../mocks/users';

@Injectable()
export class UserService {

  public users: Array<User>;
  public onNewUser$ = new EventEmitter();

  constructor() {
    this.users = mock;
  }

  public index() {
    // if (this.useMocks) {
      return Observable.of(this.users);
    // }

    // return this.httpClient('asdasd');
  }

  public createUser(user, password) {
    const data = new User(user);
    this.users.push(data);
    return Observable.of(data);
  }

  public getUser(id: number) {
    console.log(typeof id);
    return this.users.find(user => user.id === id);
  }
  public updateUser(user) {
    return Observable.of(user);
  }

  public deleteUser(id) {
    return Observable.of(id);
  }
}
