import {Injectable, EventEmitter} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {User} from '../models/User';

@Injectable()
export class AuthService {

  public onLogin$ = new EventEmitter();

  constructor() {}

  public getUser() {
    return Observable.of({firstName: 'PENETRATOR', lastName: '3000', email: 'penetrator3000@test.com'});
  }
  public login(credentials) {
    if (true) {
      this.saveToLocalStorage(new User(
        {email: credentials.email, firstName: 'PENETRATOR', lastName: '3000', isAdmin: true}));
      this.onLogin$.emit();
    }
    return Observable.of(credentials);
  }

  public saveToLocalStorage(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getFromLocalStorage(): null | User {
    const user = localStorage.getItem('user');

    if (user) {
      return new User(JSON.parse(user));
    }

    return null;
  }

  public isLoggedIn() {
    return this.getFromLocalStorage() !== null;
  }

  public sendMail(address) {
    console.log(address);
  }

  public removeFromLocalStorage() {
    localStorage.removeItem('user');
  }

  public updatePassword(data) {
    return Observable.of(data);
  }
}
