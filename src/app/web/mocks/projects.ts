export default [{
  "id": 1,
  "title": "MFA Financial, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Shawna",
  "taken": "Horatius"
}, {
  "id": 2,
  "title": "Bank of America Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Robb",
  "taken": "Lenci"
}, {
  "id": 3,
  "title": "Gabelli Equity Trust, Inc. (The)",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Hunt",
  "taken": "Gregg"
}, {
  "id": 4,
  "title": "Imprimis Pharmaceuticals, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Kat",
  "taken": "Bebe"
}, {
  "id": 5,
  "title": "Agenus Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Faustina",
  "taken": "Ronnie"
}, {
  "id": 6,
  "title": "EnLink Midstream, LLC",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Lorain",
  "taken": "La verne"
}, {
  "id": 7,
  "title": "Cemtrex Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Nessy",
  "taken": "Marrilee"
}, {
  "id": 8,
  "title": "First Trust Nasdaq Food & Beverage ETF",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Wren",
  "taken": "Hersh"
}, {
  "id": 9,
  "title": "JMP Group LLC",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Analise",
  "taken": "Cooper"
}, {
  "id": 10,
  "title": "Rogers Communication, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Jacintha",
  "taken": "Risa"
}, {
  "id": 11,
  "title": "MGC Diagnostics Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Garnette",
  "taken": "Irwin"
}, {
  "id": 12,
  "title": "Vanguard International High Dividend Yield ETF",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Gennie",
  "taken": "Shir"
}, {
  "id": 13,
  "title": "Prana Biotechnology Ltd",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Kristian",
  "taken": "Poppy"
}, {
  "id": 14,
  "title": "First Trust High Income Long Short Fund",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Ammamaria",
  "taken": "Cathyleen"
}, {
  "id": 15,
  "title": "Mid-America Apartment Communities, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Doretta",
  "taken": "Rey"
}, {
  "id": 16,
  "title": "Yatra Online, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Illa",
  "taken": "Ugo"
}, {
  "id": 17,
  "title": "Senior Housing Properties Trust",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Catlaina",
  "taken": "Hodge"
}, {
  "id": 18,
  "title": "Marine Petroleum Trust",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Doralynne",
  "taken": "Nan"
}, {
  "id": 19,
  "title": "First Trust RiverFront Dynamic Developed International ETF",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Kerstin",
  "taken": "Ethelred"
}, {
  "id": 20,
  "title": "Howard Hughes Corporation (The)",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Boyce",
  "taken": "Farrand"
}, {
  "id": 21,
  "title": "Semtech Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Merle",
  "taken": "Aubrette"
}, {
  "id": 22,
  "title": "Gorman-Rupp Company (The)",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Emilee",
  "taken": "Rosalyn"
}, {
  "id": 23,
  "title": "Innophos Holdings, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Husain",
  "taken": "Penni"
}, {
  "id": 24,
  "title": "Oxford Lane Capital Corp.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Stefanie",
  "taken": "Clea"
}, {
  "id": 25,
  "title": "Washington Federal, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Uriel",
  "taken": "Gill"
}, {
  "id": 26,
  "title": "DXP Enterprises, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Mara",
  "taken": "Merlina"
}, {
  "id": 27,
  "title": "MDU Resources Group, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Kaitlynn",
  "taken": "Dorry"
}, {
  "id": 28,
  "title": "Arbutus Biopharma Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Kelila",
  "taken": "Sheree"
}, {
  "id": 29,
  "title": "Restaurant Brands International Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Field",
  "taken": "Brig"
}, {
  "id": 30,
  "title": "United Therapeutics Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Carline",
  "taken": "Morena"
}, {
  "id": 31,
  "title": "Aerohive Networks, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Laraine",
  "taken": "Vassily"
}, {
  "id": 32,
  "title": "Blackrock MuniYield Investment QualityFund",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Katleen",
  "taken": "Ilysa"
}, {
  "id": 33,
  "title": "SiteOne Landscape Supply, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Diane-marie",
  "taken": "Giovanna"
}, {
  "id": 34,
  "title": "Eaton Vance Enhanced Equity Income Fund II",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Addy",
  "taken": "Moira"
}, {
  "id": 35,
  "title": "Siliconware Precision Industries Company, Ltd.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Tamara",
  "taken": "Federica"
}, {
  "id": 36,
  "title": "Washington Prime Group Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Norine",
  "taken": "Xymenes"
}, {
  "id": 37,
  "title": "J P Morgan Chase & Co",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Grant",
  "taken": "Hogan"
}, {
  "id": 38,
  "title": "Grupo Aeroportuario del Centro Norte S.A.B. de C.V.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Irv",
  "taken": "Averil"
}, {
  "id": 39,
  "title": "Voya Financial, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Nanni",
  "taken": "Dannye"
}, {
  "id": 40,
  "title": "Piedmont Office Realty Trust, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Ugo",
  "taken": "Koral"
}, {
  "id": 41,
  "title": "Eastman Kodak Company",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Brnaby",
  "taken": "Pepita"
}, {
  "id": 42,
  "title": "ANSYS, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Karina",
  "taken": "Den"
}, {
  "id": 43,
  "title": "FinTech Acquisition Corp. II",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Seline",
  "taken": "Conny"
}, {
  "id": 44,
  "title": "Wendy&#39;s Company (The)",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Susana",
  "taken": "Annalise"
}, {
  "id": 45,
  "title": "Rosehill Resources Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Cesare",
  "taken": "Frayda"
}, {
  "id": 46,
  "title": "Forum Merger Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Dannie",
  "taken": "Ilaire"
}, {
  "id": 47,
  "title": "Highwoods Properties, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Wolfgang",
  "taken": "Gale"
}, {
  "id": 48,
  "title": "Triumph Bancorp, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Margalo",
  "taken": "Hillary"
}, {
  "id": 49,
  "title": "Iconix Brand Group, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Latia",
  "taken": "Betty"
}, {
  "id": 50,
  "title": "AMERISAFE, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Gerti",
  "taken": "Sybila"
}, {
  "id": 51,
  "title": "FARO Technologies, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Ashla",
  "taken": "Oralee"
}, {
  "id": 52,
  "title": "Digimarc Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Mandi",
  "taken": "Thorpe"
}, {
  "id": 53,
  "title": "BiondVax Pharmaceuticals Ltd.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Cloris",
  "taken": "Grant"
}, {
  "id": 54,
  "title": "Realty Income Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Berny",
  "taken": "Leonard"
}, {
  "id": 55,
  "title": "Chesapeake Energy Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Sabina",
  "taken": "Ermentrude"
}, {
  "id": 56,
  "title": "The Hanover Insurance Group, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Marlyn",
  "taken": "Jackson"
}, {
  "id": 57,
  "title": "iShares S&P/Citigroup 1-3 Year International Treasury Bond Fun",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Elsi",
  "taken": "Thekla"
}, {
  "id": 58,
  "title": "AEterna Zentaris Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Jemimah",
  "taken": "Charla"
}, {
  "id": 59,
  "title": "Stanley Black & Decker, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Timothee",
  "taken": "Clerkclaude"
}, {
  "id": 60,
  "title": "Hawaiian Holdings, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Hilliard",
  "taken": "Crosby"
}, {
  "id": 61,
  "title": "Student Transportation Inc",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Swen",
  "taken": "Alyse"
}, {
  "id": 62,
  "title": "Prudential Financial, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Edward",
  "taken": "Justus"
}, {
  "id": 63,
  "title": "Greenhill & Co., Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Kym",
  "taken": "Jamal"
}, {
  "id": 64,
  "title": "STRATS Trust",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Amos",
  "taken": "Corbie"
}, {
  "id": 65,
  "title": "Brookfield DTLA Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Bing",
  "taken": "Alyson"
}, {
  "id": 66,
  "title": "HRG Group, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Lorain",
  "taken": "Jerrilee"
}, {
  "id": 67,
  "title": "Minerva Neurosciences, Inc",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Reider",
  "taken": "Dar"
}, {
  "id": 68,
  "title": "Grupo Aeroportuario del Sureste, S.A. de C.V.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Adela",
  "taken": "Ruthe"
}, {
  "id": 69,
  "title": "Timken Company (The)",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Lenora",
  "taken": "Eadie"
}, {
  "id": 70,
  "title": "Vanguard Russell 1000 Value ETF",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Thedrick",
  "taken": "Etan"
}, {
  "id": 71,
  "title": "Gladstone Investment Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Rosamund",
  "taken": "Saidee"
}, {
  "id": 72,
  "title": "Copart, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Dredi",
  "taken": "Jess"
}, {
  "id": 73,
  "title": "Winnebago Industries, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Alisa",
  "taken": "Meara"
}, {
  "id": 74,
  "title": "Halcon Resources Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Isaac",
  "taken": "Reade"
}, {
  "id": 75,
  "title": "Jensyn Acquistion Corp.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Alli",
  "taken": "Regina"
}, {
  "id": 76,
  "title": "Sinclair Broadcast Group, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Jenica",
  "taken": "Merry"
}, {
  "id": 77,
  "title": "Ally Financial Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Glenine",
  "taken": "Eugene"
}, {
  "id": 78,
  "title": "KVH Industries, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Iggie",
  "taken": "Britney"
}, {
  "id": 79,
  "title": "RLI Corp.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Dede",
  "taken": "Korella"
}, {
  "id": 80,
  "title": "Third Point Reinsurance Ltd.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Bard",
  "taken": "Richy"
}, {
  "id": 81,
  "title": "Renewable Energy Group, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Norene",
  "taken": "Ernesta"
}, {
  "id": 82,
  "title": "Ascendis Pharma A/S",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Ursola",
  "taken": "Curcio"
}, {
  "id": 83,
  "title": "Mexico Fund, Inc. (The)",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Elwyn",
  "taken": "Alverta"
}, {
  "id": 84,
  "title": "Community West Bancshares",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Felix",
  "taken": "Thayne"
}, {
  "id": 85,
  "title": "Natuzzi, S.p.A.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Roma",
  "taken": "Vidovik"
}, {
  "id": 86,
  "title": "Western Asset Investment Grade Defined Opportunity Trust Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Shadow",
  "taken": "Dion"
}, {
  "id": 87,
  "title": "Community West Bancshares",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Annmarie",
  "taken": "Bunni"
}, {
  "id": 88,
  "title": "Beneficial Bancorp, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Goober",
  "taken": "Darcy"
}, {
  "id": 89,
  "title": "Bank of the Ozarks",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Freda",
  "taken": "Zacharia"
}, {
  "id": 90,
  "title": "Lumos Networks Corp.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Fin",
  "taken": "Alyosha"
}, {
  "id": 91,
  "title": "The Bon-Ton Stores, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Clementine",
  "taken": "Jeralee"
}, {
  "id": 92,
  "title": "Enel Chile S.A.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Arlyne",
  "taken": "Glenn"
}, {
  "id": 93,
  "title": "Credit Acceptance Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Vladimir",
  "taken": "Eleanora"
}, {
  "id": 94,
  "title": "Nuveen S&P 500 Buy-Write Income Fund",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Myrna",
  "taken": "Verina"
}, {
  "id": 95,
  "title": "Global Indemnity Limited",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Edgard",
  "taken": "Rosie"
}, {
  "id": 96,
  "title": "Yum! Brands, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Franny",
  "taken": "Cinnamon"
}, {
  "id": 97,
  "title": "Crown Crafts, Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Willy",
  "taken": "Cullan"
}, {
  "id": 98,
  "title": "DSW Inc.",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Ellynn",
  "taken": "Paulina"
}, {
  "id": 99,
  "title": "National Holdings Corporation",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Lora",
  "taken": "Lemmie"
}, {
  "id": 100,
  "title": "Central Valley Community Bancorp",
  "status": "in_progress",
  "effort": "12/12",
  "spent": 12,
  "approved": 12,
  "assigned": "Avictor",
  "taken": "Noble"
}];