export class Project {

  public constructor(data: any = {}) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        this[key] = data[key];
      }
    }
  }

  public id: Number;
  public title: string;
  public status: string;
  public effort: string;
  public spent: Number;
  public approved: Number;
  public assigned: string;
  public taken: string;
}
