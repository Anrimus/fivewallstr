export class User {


  public id: Number;
  public email: String;
  public firstName: String;
  public lastName: String;
  public isAdmin: boolean;

  public constructor(data: any = {}) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        this[key] = data[key];
      }
    }
  }
}
