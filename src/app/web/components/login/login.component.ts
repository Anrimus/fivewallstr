import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {CustomErrorMatcher} from '../../../CustomErrorMatcher';
import {FormGeneratorService} from '../../services/form-generator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public credentials = {
    email: '',
    password: ''
  };

  protected emailFormControl = this.formGenerator.RequiredEmailService();

  protected passwordFormControl = this.formGenerator.RequiredService();

  protected matcher = new CustomErrorMatcher();

  constructor(
    protected authService: AuthService,
    protected router: Router,
    protected formGenerator: FormGeneratorService
  ) {}

  ngOnInit() {
  }

  public login() {
    this.authService.login(this.credentials).subscribe(
      () => this.router.navigate(['/']));
  }
}
