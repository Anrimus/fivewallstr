import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/User';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  public user: User = null;

  constructor(
      protected authService: AuthService,
  ) {
    this.user = this.authService.getFromLocalStorage();
  }

  ngOnInit() {
    this.authService.onLogin$.subscribe(() => this.user = this.authService.getFromLocalStorage());
  }

  public logout() {
    this.authService.removeFromLocalStorage();
    this.user = null;
  }
}
