import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/User';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public user: User = null;

  constructor(
    protected authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.user = this.authService.getFromLocalStorage();
  }
}
