import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {CustomErrorMatcher} from '../../../CustomErrorMatcher';
import {FormGeneratorService} from '../../services/form-generator.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  protected email: string;

  protected emailFormControl = this.formGenerator.RequiredEmailService();

  protected matcher = new CustomErrorMatcher();

  constructor(protected router: Router,
              protected authService: AuthService,
              protected formGenerator: FormGeneratorService) { }

  ngOnInit() {
  }

  public sendMail() {
    this.authService.sendMail(this.email);
    this.router.navigate(['/update-password']);
  }
}

