import {Component, Inject, OnInit} from '@angular/core';
import {ProjectService} from '../../services/project.service';
import {UserService} from '../../services/users.service';
import {User} from '../../models/User';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {CustomErrorMatcher} from '../../../CustomErrorMatcher';
import {FormGeneratorService} from '../../services/form-generator.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {


  public project = {title: '', status: '', effort: '', spent: '', approved: '', assigned: '', taken: ''};
  public statuses = ['done', 'in progress', 'waiting'];
  public users: Observable<User[]>;
  public dontMatch = false;

  protected titleFormControl = this.formGenerator.RequiredService();

  protected statusFormControl = this.formGenerator.RequiredService();

  protected effortFormControl = this.formGenerator.RequiredPatternService(/^\d+\/\d+$/);

  protected spentFormControl = this.formGenerator.RequiredService();

  protected approvedFormControl = this.formGenerator.RequiredService();

  protected assignedFormControl = this.formGenerator.RequiredService();

  protected takenFormControl = this.formGenerator.RequiredService();

  protected matcher = new CustomErrorMatcher();

  constructor(protected projectService: ProjectService,
              protected userService: UserService,
              protected router: Router,
              protected formGenerator: FormGeneratorService,
              public dialogRef: MatDialogRef<CreateProjectComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.users = this.userService.index();
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public submit() {
    const effort_parts = this.project.effort.split('/');
    this.dontMatch = effort_parts[1] !== this.project.spent.toString();
    console.log(this.dontMatch);
    if (!this.dontMatch) {
      this.projectService.createNewProject(this.project);
      this.projectService.onProjectCreate$.emit();
      this.snackBar.open('project successfully created!', null, {
        duration: 2000,
      });
      this.dialogRef.close('');
      this.router.navigate(['projects']);
    }
  }
}
