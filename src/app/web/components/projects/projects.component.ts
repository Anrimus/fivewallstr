import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import {Project} from '../../models/Project';
import {ProjectService} from '../../services/project.service';
import {CreateProjectComponent} from '../create-project/create-project.component';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements AfterViewInit, OnInit {
  protected displayedColumns = ['title', 'status', 'effort', 'spent', 'approved', 'assigned', 'taken'];
  protected dataSource: MatTableDataSource<Project>;
  protected projects: Array<Project>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(protected projectService: ProjectService,
              public dialog: MatDialog) {
    this.projectService.getProjects().subscribe((response) => this.projects = response);
    this.dataSource = new MatTableDataSource(this.projects);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.projectService.onProjectCreate$.subscribe(() => this.projectService.getProjects().subscribe((response) => {
      this.projects = response;
    }));
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(CreateProjectComponent, {hasBackdrop: true});

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
