import {Component, Inject, OnInit} from '@angular/core';
import {CustomErrorMatcher} from '../../../CustomErrorMatcher';
import {UserService} from '../../services/users.service';
import {FormGeneratorService} from '../../services/form-generator.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {CreateProjectComponent} from '../create-project/create-project.component';
import {Router} from '@angular/router';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  protected user = {firstName: '', lastName: '', email: '', isAdmin: false};

  protected password: String;

  protected passwordFormControl = this.formGenerator.RequiredService();

  protected firstNameFormControl = this.formGenerator.RequiredService();

  protected lastNameFormControl = this.formGenerator.RequiredService();

  protected emailFormControl = this.formGenerator.RequiredEmailService();

  protected matcher = new CustomErrorMatcher();

  constructor(protected userService: UserService,
              protected formGenerator: FormGeneratorService,
              public dialogRef: MatDialogRef<CreateProjectComponent>,
              public snackBar: MatSnackBar,
              protected router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public submit() {
    this.userService.createUser(this.user, this.password);
    this.userService.onNewUser$.emit();
    this.snackBar.open('user successfully created!', null, {
      duration: 2000,
    });
    this.dialogRef.close('');
    this.router.navigate(['users']);

  }
}
