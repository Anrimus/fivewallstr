import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FormGeneratorService} from '../../services/form-generator.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {

  public password = '';
  public confirm = '';
  public email = '';

  protected passwordFormControl = this.formGenerator.RequiredService();

  protected confirmPasswordFormControl = this.formGenerator.RequiredService();

  constructor(protected authService: AuthService,
              protected router: Router,
              protected formGenerator: FormGeneratorService) {
  }

  ngOnInit() {
  }

  public submit() {
    this.authService.updatePassword({password: this.password, email: this.email})
      .subscribe(response => {
        this.authService.login({password: this.password, email: this.email});
        this.router.navigate(['/']);
      });
  }
}
