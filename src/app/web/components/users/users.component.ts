import {UserService} from '../../services/users.service';
import {AfterViewInit, Component, ViewChild, OnInit} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource, MatSnackBar} from '@angular/material';
import {User} from '../../models/User';
import {CreateUserComponent} from '../create-user/create-user.component';
import {UpdateUserComponent} from '../update-user/update-user.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  protected displayedColumns = ['firstName', 'lastName', 'email', 'isAdmin', 'update', 'delete'];
  protected dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public userService: UserService,
              public dialog: MatDialog,
              protected snackBar: MatSnackBar) {
    this.getUsers();
  }

  ngOnInit() {
    this.userService.onNewUser$.subscribe(() => {
        this.getUsers();
        console.log('EMITTED!');
      }
    );
  }

  protected getUsers() {
    this.userService.index().subscribe(response => {
      this.dataSource = new MatTableDataSource(response);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  protected applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  protected openCreateDialog(): void {
    const dialogRef = this.dialog.open(CreateUserComponent, {hasBackdrop: true});

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  protected updateDialog(id) {
    console.log(this.userService.getUser(id));
    const dialogRef = this.dialog.open(UpdateUserComponent, {hasBackdrop: true, data: {user: this.userService.getUser(id)}});
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  protected delete(id) {
    this.snackBar.open('user successfully deleted!', null, {
      duration: 2000,
    });
  }
}
