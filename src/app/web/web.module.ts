import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';

import {routes} from './web.routes';

import {AuthService} from './services/auth.service';
import {ProjectService} from './services/project.service';
import {UserService} from './services/users.service';
import {FormGeneratorService} from './services/form-generator.service';

import {AuthGuard} from './guards/auth.guard';
import {GuestGuard} from './guards/guest.guard';
import {AdminGuard} from './guards/admin.guard';

import {MainComponent} from './components/main/main.component';
import {LoginComponent} from './components/login/login.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {HeaderComponent} from './components/header/header.component';
import {ProjectsComponent} from './components/projects/projects.component';
import {UsersComponent} from './components/users/users.component';
import {UpdatePasswordComponent} from './components/update-password/update-password.component';
import {CreateProjectComponent} from './components/create-project/create-project.component';
import {CreateUserComponent} from './components/create-user/create-user.component';
import {HomeComponent} from './components/home/home.component';
import {MAT_DIALOG_DEFAULT_OPTIONS, MAT_DIALOG_DATA} from '@angular/material';
import {UpdateUserComponent} from './components/update-user/update-user.component';


@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    MainComponent,
    LoginComponent,
    NavbarComponent,
    ForgotPasswordComponent,
    HeaderComponent,
    UsersComponent,
    ProjectsComponent,
    UpdatePasswordComponent,
    CreateProjectComponent,
    CreateUserComponent,
    HomeComponent,
    UpdateUserComponent
  ],
  exports: [
    MainComponent
  ],
  providers: [
    AuthService,
    ProjectService,
    AuthGuard,
    GuestGuard,
    AdminGuard,
    UserService,
    FormGeneratorService,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
    {provide: MAT_DIALOG_DATA, useValue: {hasBackdrop: false}},
  ],
  entryComponents: [
    CreateProjectComponent,
    CreateUserComponent
  ],
})
export class WebModule {
}
