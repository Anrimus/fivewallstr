import {Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {GuestGuard} from './guards/guest.guard';
import {AuthGuard} from './guards/auth.guard';
import {ProjectsComponent} from './components/projects/projects.component';
import {UsersComponent} from './components/users/users.component';
import {AdminGuard} from './guards/admin.guard';
import {UpdatePasswordComponent} from './components/update-password/update-password.component';
import {HomeComponent} from './components/home/home.component';
import {UpdateUserComponent} from './components/update-user/update-user.component';

const routes: Routes = [

  {path: 'login', component: LoginComponent, canActivate: [GuestGuard]},

  {path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [GuestGuard]},

  {path: 'update-password', component: UpdatePasswordComponent, canActivate: [GuestGuard]},

  {
    path: '', component: NavbarComponent, canActivate: [AuthGuard], children: [

      {path: '', component: HomeComponent},

      {path: 'projects', component: ProjectsComponent},

      {path: 'users', component: UsersComponent, canActivate: [AdminGuard]},

      {path: 'users/:id', component: UpdateUserComponent, canActivate: [AdminGuard]},
    ]
  },

  {path: '**', redirectTo: ''},

];

export {routes};
