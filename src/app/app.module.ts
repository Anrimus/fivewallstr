import {NgModule} from '@angular/core';
import {WebModule} from './web/web.module';

import {AppComponent} from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    WebModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
